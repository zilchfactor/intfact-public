#ifndef __COMPLEMENT__
#define __COMPLEMENT__
#include <string>
using namespace std;

std::string complementOf(std::string matching_tuple) {
	std::string new_matching_tuple;
	for (int x = 0; x < matching_tuple.size(); ++x) {
		new_matching_tuple += boost::lexical_cast<std::string>(1-(matching_tuple[x]-'0'));
	}
	return new_matching_tuple;
}
#endif
