#ifndef __MATCH__
#define __MATCH__
#include <string>
#include <string.h>
#include <vector>
#include "helper.hpp"
using namespace std;

bool determineHaltCondition(vector<int>* match_positions, vector<int>** forward_match_positions, int l, int j) {
	if (!forward_match_positions[j]) return false;
	for (int m = 0; m < match_positions->size();++m) {
		if (std::find(forward_match_positions[j]->begin(), forward_match_positions[j]->end(), match_positions->at(m)) != forward_match_positions[j]->end()) continue;
		else {
			return false;
		}
	}
	return true;
}

std::string correctMatchingTuple(std::string temp_matching_tuple, vector<int>* match_positions, vector<int>** forward_match_positions, int j) {
	if (j==0) return temp_matching_tuple;
	vector<int>* previous_match_position = forward_match_positions[j-1];
	if (!previous_match_position) return temp_matching_tuple;
	int upper_limit_prev_match = previous_match_position->at(previous_match_position->size()-1);
	int lower_limit_prev_match = previous_match_position->at(0);
	int upper_limit_current_match = match_positions->at(match_positions->size()-1);
	int lower_limit_current_match = match_positions->at(0);
	int m = 0;
	std::string corrected_match_string = "";
	if (upper_limit_current_match >= lower_limit_prev_match || lower_limit_current_match <= upper_limit_prev_match) {
		//overlap
		if (upper_limit_current_match >= lower_limit_prev_match) {
			for (int i = lower_limit_current_match; i <= lower_limit_prev_match; ++i) {
				corrected_match_string+=temp_matching_tuple[m++];
			}
		} else if (lower_limit_current_match <= upper_limit_prev_match) {
			for (int i = upper_limit_prev_match; i <= upper_limit_current_match; ++i) {
				corrected_match_string += temp_matching_tuple[m++];
			}
		}
	}
	return corrected_match_string;
}

#define MAX_BIT_LENGTH 7
bool matchTarget(vector<string> rnum, vector<string> bnum, int start_index, int l, vector<string> n_forward_tuple, vector<string> n_backward_tuple, vector<string> r_forward_tuple, vector<string> r_backward_tuple, vector<int>**& forward_match_positions, vector<int>** reverse_match_positions, std::string& matching_tuple, bool& terminatingCriteria, bool& match_context) {
	std::string forward_accumulator = "";
	std::string reverse_accumulator = "";
	vector<int>* match_positions = new vector<int>();
	//Memory Leak here
	std::string temp_matching_tuple = "";
	bool match_success = false;
	int j = start_index;
	for ( ; j < start_index+MAX_BIT_LENGTH; ++j) {
		if (j == l) break;
		forward_accumulator += bnum[j];
		if (forward_accumulator.size() > MAX_BIT_LENGTH) break;
		match_positions->push_back(j);
		for (int k = start_index; k <= j; ++k ) {
			if (n_forward_tuple[k] == forward_accumulator) {
				match_success = true;
				temp_matching_tuple = n_forward_tuple[k];
				break;
			}
			if (n_backward_tuple[k] == forward_accumulator) {
				match_success = true;
				temp_matching_tuple = n_backward_tuple[k];
				break;
			}
			if (r_forward_tuple[k] == forward_accumulator) {
				match_success = true;
				match_context = false;
				temp_matching_tuple = complementOf(r_forward_tuple[k]);
				break;
			}
			if (r_backward_tuple[k] == forward_accumulator) {
				match_success = true;
				match_context = false;
				temp_matching_tuple = complementOf(r_backward_tuple[k]);
				break;
			}
		} 
		if (match_success) {
			std::reverse(temp_matching_tuple.begin(), temp_matching_tuple.end());
			break;
		}
	}
	if (!match_success) {
		j = start_index;
		for ( ; j < start_index+MAX_BIT_LENGTH; ++j) {
			if (j == l) break;
			reverse_accumulator += rnum[j];
			if (reverse_accumulator.size() > MAX_BIT_LENGTH) break;
			match_positions->push_back(j);
			for (int k = start_index; k <= j; ++k ) {
				if (n_forward_tuple[k] == reverse_accumulator) {
					match_success = true;
					match_context = false;
					temp_matching_tuple = complementOf(n_forward_tuple[k]);
					break;
				}
				if (n_backward_tuple[k] == reverse_accumulator) {
					match_success = true;
					match_context = false;
					temp_matching_tuple = complementOf(n_backward_tuple[k]);
					break;
				}
				if (r_forward_tuple[k] == reverse_accumulator) {
					match_success = true;
					temp_matching_tuple = r_forward_tuple[k];
					break;
				}
				if (r_backward_tuple[k] == reverse_accumulator) {
					match_success = true;
					temp_matching_tuple = r_backward_tuple[k];
					break;
				}
			} 
			if (match_success) {
				std::reverse(temp_matching_tuple.begin(), temp_matching_tuple.end());
				break;
			}
		}
	}
	if (match_success) {
		matching_tuple = correctMatchingTuple(temp_matching_tuple, match_positions, forward_match_positions, j); 
		forward_match_positions[j] = match_positions;
		terminatingCriteria = determineHaltCondition(match_positions, forward_match_positions, l, j);
	}
	return match_success;
}
#endif
