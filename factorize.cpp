//Algorithm Intfact : An Algorithm to Factor Arbitrary Precision Integers in Real Time
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <string>
#include <boost/lexical_cast.hpp>
#include <gmp.h>
#include "bcodes.hpp"
#include "init.hpp"

using namespace std;
using namespace boost;

std::string convertBinaryToDecimal(std::string factor) {
	std::string decimal_string = "";
	mpz_t num_z;
	mpz_init(num_z);
	mpz_set_si(num_z, 0);
	mpz_t prod_z;
	mpz_init(prod_z);
	mpz_set_ui(prod_z, 1);
	mpz_t term_z;
	mpz_init(term_z);
	int l = factor.size();
	for (int i = l - 1; i >= 0; --i) {
		int nk = factor[i]-'0';
		mpz_set_ui(term_z, nk);
		mpz_mul(term_z, term_z, prod_z);
		mpz_mul_ui(prod_z, prod_z,2);
		mpz_add(num_z, num_z, term_z);
	}
	decimal_string = strdup(mpz_get_str(0, 10, num_z));
	mpz_clear(num_z);
	mpz_clear(term_z);
	mpz_clear(prod_z);
	return decimal_string;
}

char* morph(char* num) {
	int l = strlen(num);
	char* mnum = new char[2*l+1];
	for (int i = 0; i < 2*l; i+=2) {
		mnum[i] = '2';
		mnum[i+1] = num[i/2] ;
	}
	mnum[2*l]  = '\0';
	return mnum;
}

int main(int argc, char* argv[]) {
	//read from command line the number to be factored
	char*  num = strdup(argv[1]);
	int l = strlen(num);
	num = (char*) (morph(num));
	//printf("\n Number entered was : \t%s\n", num);
	vector<string> bnum;
	vector<string> n_forward_tuple;
	init(num, bnum, n_forward_tuple);
	for (int i = 0; i < n_forward_tuple.size(); ++i) {
		cout << n_forward_tuple[i] << " , ";
	}
	cout << endl;
}
