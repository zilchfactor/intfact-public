#ifndef __TIME_TRAVERSAL__
#define __TIME_TRAVERSAL__
#include <string>
#include <string.h>
#include <vector>
#include "match.hpp"
#include "helper.hpp"
using namespace std;

std::string TimeTraversal(char* num, vector<string> bnum, vector<string> rnum, vector<string> n_forward_tuple, vector<string> n_backward_tuple, vector<string> r_forward_tuple, vector<string> r_backward_tuple, vector<int>**& forward_match_positions, vector<int>** reverse_match_positions, bool& halt ) {
    int l = strlen(num);
    bool match_context = true; //same stream match; false : cross stream match
    bool last_match_context = true;
    bool terminatingCriteria = false;
    std::string factor = "";
    for (int i = 0; i < l; ++i) {
	    //For match target in straight num
	    std::string matching_tuple = "";
	    last_match_context = match_context;
	    bool match_success = matchTarget(bnum, rnum, i, l, n_forward_tuple, n_backward_tuple, r_forward_tuple, r_backward_tuple, forward_match_positions, reverse_match_positions, matching_tuple, terminatingCriteria, match_context);
	    if (match_success) {
		    if (last_match_context == match_context) {
			    matching_tuple = complementOf(matching_tuple);
	            }
		    factor += matching_tuple; 
			if (terminatingCriteria) {
				halt = true;
			}
		        return factor;
	    }
	    //For match target in reverse of num
	    if (!match_success) {
	        last_match_context = match_context;
	        match_success = matchTarget(rnum, bnum, i, l, n_forward_tuple, n_backward_tuple, r_forward_tuple, r_backward_tuple, forward_match_positions, reverse_match_positions, matching_tuple, terminatingCriteria, match_context);
		if (match_success) {
		        if (last_match_context == match_context) {
			      matching_tuple = complementOf(matching_tuple);
	                }
			factor += matching_tuple;
			if (terminatingCriteria) {
				halt = true;
			}
		        return factor;
		}
	        if (!match_success) 
		    continue;
	    }
    }
}
#endif
