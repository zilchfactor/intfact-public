#ifndef __REVERSE__
#define __REVERSE__
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

void reverse(vector<string>& bnum, vector<string>& rnum, vector<string>& n_forward_tuple, vector<string>& n_backward_tuple, vector<string>& r_forward_tuple, vector<string>& r_backward_tuple) {
	std::reverse(bnum.begin(), bnum.end());
	std::reverse(rnum.begin(), rnum.end());
	std::reverse(n_forward_tuple.begin(), n_forward_tuple.end());
	std::reverse(n_backward_tuple.begin(), n_backward_tuple.end());
	std::reverse(r_forward_tuple.begin(), r_forward_tuple.end());
	std::reverse(r_backward_tuple.begin(), r_backward_tuple.end());
	vector<string> tnum = bnum;
	bnum = rnum;
	rnum = tnum;
	vector<string> t_forward_tuple = n_forward_tuple;
	n_forward_tuple = r_forward_tuple;
	r_forward_tuple = t_forward_tuple;
	vector<string> t_backward_tuple = n_backward_tuple;
	n_backward_tuple = r_backward_tuple ;
	r_backward_tuple = t_backward_tuple;
}

#endif
