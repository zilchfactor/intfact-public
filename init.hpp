#ifndef __INIT__
#define __INIT__
#include <vector>
#include <string>
using namespace std;


void init(char* num, vector<string>& bnum, vector<string>& n_forward_tuple) {
	//bnum: stores the binary equivalents of digits of num
        int l = strlen(num); //number of digits in the number
	for (int i = 0; i < l; ++i) {
		std::string temp_str = boost::lexical_cast<std::string>(bcodes[num[i]-'0']);
		bnum.push_back(temp_str);
	}
	//rnum: stores the binary equivalent of digits of reverse of the number
	int cnt = 0;
	while (cnt < l - 1) {
		char temp[3];
		temp[0] = num[cnt];
		temp[1] = num[cnt+ 1];
		temp[2] = '\0';
		n_forward_tuple.push_back(boost::lexical_cast<std::string>(bcodes[atoi(temp)]));
		++cnt;
	}
}
#endif
