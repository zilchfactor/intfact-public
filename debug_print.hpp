#ifndef __DEBUG_PRINT__
#define __DEBUG_PRINT__
#include <string>
#include <string.h>
#include <vector>
using namespace std;

void debug_print(char* num, vector<string> bnum, vector<string> rnum, vector<string> n_forward_tuple, vector<string> n_backward_tuple, vector<string> r_forward_tuple, vector<string> r_backward_tuple) {
	int l = strlen(num);
	for (int i = 0; i < l; ++i) {
		cout << num[i] << " , ";
	}
	cout << endl;
	for (int i = 0; i < l; ++i) {
		cout << bnum[i] << " , ";
	}
	cout << endl;
	for (int i = 0; i < l; ++i) {
		cout << n_forward_tuple[i] << " , ";
	}
	cout << endl;
	for (int i = 0; i < l; ++i) {
		cout << n_backward_tuple[i] << " , ";
	}
	cout << endl;
	cout << endl;
	for (int i = 0; i < l; ++i) {
		cout << num[l-1-i] << " , ";
	}
	cout << endl;
	for (int i = 0; i < l; ++i) {
		cout << rnum[i] << " , ";
	}
	cout << endl;
	for (int i = 0; i < l; ++i) {
		cout << r_forward_tuple[i] << " , ";
	}
	cout << endl;
	for (int i = 0; i < l; ++i) {
		cout << r_backward_tuple[i] << " , ";
	}
	cout << endl;
	cout << endl;
}
#endif
